<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Questrial&display=swap" rel="stylesheet">
<style>

h1 {
    text-align: center;
    margin-top: 3vh;
    font-family: 'Questrial', sans-serif;
}

.container {
    border: 3px solid #000;
    border-radius: 10px;
    width: 25vw;
    margin-left: auto;
    margin-right: auto;
    margin-top: 15vh;
}

.container-server {
    border: 3px solid #000;
    text-align: center;
    width: 50vh;
    margin-left: auto;
    margin-right: auto;
    padding: 15px;
    border-radius: 10px;
    margin-top: 5vh;
    overflow: auto;
}

p {
    font-family: 'Questrial', sans-serif;
    font-weight: bold;
    text-decoration: underline;
}

h2 {
    text-align: center;
    font-family: 'Questrial', sans-serif;
}

a {
    display: flex;
    flex-direction: column;
    margin: 50px;
    text-decoration: none;
    background-color: #000;
    box-shadow: 2px 5px 5px grey;
    width: 15vw;
    height: 3vh;
    font-size: 19px;
    border-radius: 5px;
    margin-left: auto;
    margin-right: auto;
    padding-top: 13px;
    text-align: center;
    color: white;
    font-family: 'Questrial', sans-serif;
}

a:hover {
    background-color: #DCDCDC;
    color: black;
}
    
</style>

<?php

const CLIENT_ID = "client_6070546c6aba63.16480463";
const CLIENT_SECRET = "38201ad253c323a79d9108f4588bbc62d2e1a5c6";
const CLIENT_FBID = "344611627223136";
const CLIENT_FBSECRET = "3f8c5cdef5f3f0c6809c06e78a31abd6";
const CLIENT_DISCORDID = "859446295432921109";
const CLIENT_DISCORDSECRET = "PjA7K_bRoT-0-hYHTMMObzUSjIfEkAKV";
const CLIENT_GOOGLEID = "866480658820-c63jsfrminj7u5k42f93qcrvttt5nt38.apps.googleusercontent.com";
const CLIENT_GOOGLESECRET = "iC0LhLAy8q9zZz0ZV8ZkWODc";

function getUser($params){
    
    $stateExploded = explode("_", $params["state"]);

    // var_dump($params);

    // var_dump($stateExploded);

    $link = '';

    switch ($stateExploded[0]){
        case 'oauth':
            $link = "http://oauth-server:8081/token?"
                    . "client_id=" . CLIENT_ID
                    . "&client_secret=" . CLIENT_SECRET;
            $result = file_get_contents($link
                    . "&" . http_build_query($params));
        break;

        case 'facebook':
            $link = "https://graph.facebook.com/oauth/access_token?"
                    . "client_id=" . CLIENT_FBID
                    . "&client_secret=" . CLIENT_FBSECRET
                    . "&redirect_uri=https://localhost/fbauth-success";
            $result = file_get_contents($link
                    . "&" . http_build_query($params));
        break;

        case 'discord':
            $link = "https://discord.com/api/oauth2/token";
            $data = [
                "client_id" => CLIENT_DISCORDID,
                "client_secret" => CLIENT_DISCORDSECRET,
                "redirect_uri" => "https://localhost/discordauth-success"
            ];
            $data = array_merge($data, $params);
            $data = http_build_query($data);

            $contextDiscord = stream_context_create([
                'http' => [
                    'method' => "POST",
                    'header' => "Content-type : application/x-www-form-urlencoded\r\n"
                        . "Content-Lenght: " . strlen($data) . "\r\n",
                    'content' => $data
                ]
            ]);
            $result = file_get_contents($link, false, $contextDiscord);
        break;

        case 'google':
            $link = "https://accounts.google.com/o/oauth2/token";
            $data = [
                "client_id" => CLIENT_GOOGLEID,
                "client_secret" => CLIENT_GOOGLESECRET,
                "redirect_uri" => "https://localhost/googleauth-success"

            ];
            $data = array_merge($data, $params);
            $data = http_build_query($data);

            $contextGoogle = stream_context_create([
                'http' => [
                    'method' => "POST",
                    'header' => "Content-type : application/x-www-form-urlencoded\r\n"
                        . "Content-Lenght: " .strlen($data) . "\r\n",
                    'content' => $data
                ]
            ]);
           $result = file_get_contents($link, false, $contextGoogle);
        break;

        default: 
            break;

    }


    $token = json_decode($result, true)["access_token"];
    // var_dump($token);

    $context = stream_context_create([
        'http' => [
            'method' => "GET", 
            'header' => "Accept: application/json\r\nAuthorization: Bearer " . $token , 
            'user_agent' => 'request'
        ]
    ]);

    // var_dump($stateExploded[0]);

    switch ($stateExploded[0]){
        case 'oauth':
            $result = file_get_contents("http://oauth-server:8081/me", false, $context);
        break;

        case 'facebook':
            $result = file_get_contents("https://graph.facebook.com/me", false, $context);
        break;

        case 'discord':
            $result = file_get_contents("https://discord.com/api/users/@me", false, $context);
        break;

        case 'google':
            $result = file_get_contents("https://openidconnect.googleapis.com/v1/userinfo", false, $context);
        break;
        
        default: 
            break;

    }

    $user = json_decode($result, true);
    echo '<h1>Utilisateur</h1>';
    echo '<div class="container-server">';
    echo '<p>Token : </p>' ;
    var_dump($token);
    echo '</br>';
    echo '<p>Données utilisateur : </p>' ;
    var_dump($user);
    echo '</div>';
}

function handleLogin() {
    $paramsFB = [
        'client_id' => CLIENT_FBID,
        'state' => uniqid('facebook_'),
        'response_type' => 'code',
        'redirect_uri' => 'https://localhost/fbauth-success',
        'scope' => 'email'
    ];

    $paramsDiscord = [
        'client_id' => CLIENT_DISCORDID,
        'state' => uniqid('discord_'),
        'response_type' => 'code',
        'redirect_uri' => 'https://localhost/discordauth-success',
        'scope' => 'identify guilds'
    ];

    $paramsGoogle = [
        'client_id' => CLIENT_GOOGLEID,
        'state' => uniqid('google_'),
        'response_type' => 'code',
        'redirect_uri' => 'https://localhost/googleauth-success',
        'scope' => 'email'
    ];
    // echo $paramsGoogle['state'];
    // var_dump($paramsGoogle);

    echo '<h1>Soutenance SDK</h1>';
    echo '<div class="container">';
    echo '<h2>Login with : </h2>';
    echo "<a href='http://localhost:8081/auth?"
        . "response_type=code"
        . "&client_id=" . CLIENT_ID
        . "&scope=basic"
        . "&state=".uniqid("oauth_")."'>Oauth-server</a>";
    
    echo "<a href='https://www.facebook.com/v2.10/dialog/oauth?" . http_build_query($paramsFB, null, '&') . "'>Facebook</a>";
    echo "<a href='https://discord.com/api/oauth2/authorize?" . http_build_query($paramsDiscord, null, '&') . "'>Discord</a>";
    echo "<a href='https://accounts.google.com/o/oauth2/v2/auth?". http_build_query($paramsGoogle, null, '&') . "'>Google</a>";
    echo "</div>";
}

function handleSuccess() {
    ["code" => $code, "state" => $state] = $_GET;
    
    getUSer([
        "grant_type" => "authorization_code",
        "code" => $code,
        "state" => $state
    ]);
}

function handleFBSuccess() {
    ["code" => $code, "state" => $state] = $_GET;

    getUser([
        "grant_type" => "authorization_code",
        "state" => $state,
        "code" => $code
    ]);
}

function handleDISCORDSuccess() {
    ["code" => $code, "state" => $state] = $_GET;

    getUser([
        "grant_type" => "authorization_code",
        "state" => $state,
        "code" => $code
    ]);
}


function handleGOOGLESuccess() {
    ["code" => $code, "state" => $state] = $_GET;

    getUser([
        "grant_type" => "authorization_code",
        "state" => $state,
        "code" => $code
    ]);
}

function handleError() {
    echo "refusé";
}

$route = strtok($_SERVER["REQUEST_URI"], '?');
switch ($route) {
    case '/login':
        handleLogin();
        break;
    case '/auth-success':
        handleSuccess();
        break;
    case '/fbauth-success':
        handleFBSuccess();
        break;
    case '/discordauth-success':
        handleDISCORDSuccess();
        break;
    case '/googleauth-success':
        handleGOOGLESuccess();
        break;
    case '/auth-error':
        handleError();
        break;
    case '/password':
        if ($_SERVER['REQUEST_METHOD'] === "GET") {
            echo "<form method='POST'>";
            echo "<input name='username'>";
            echo "<input name='password'>";
            echo "<input type='submit' value='Log with oauth'>";
            echo "</form>";
        } else {
            ['username' => $username, 'password' => $password] = $_POST;
            
            getUser([
                'grant_type' => "password",
                'username' => $username,
                'password' => $password
            ]);
        }
        break;
    default:
        http_response_code(404);
}